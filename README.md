# Aplikasi Catalog #

Membuat user untuk mengakses database

```
createuser -P catalog
Enter password for new role: 
Enter it again: 
```

Membuat database

```
createdb -Ocatalog catalogdb
```

Instalasi `metrics-server` untuk mengaktifkan metric monitoring

```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```

Load testing

```
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://aplikasi-catalog-dev/api/product/; done"
```