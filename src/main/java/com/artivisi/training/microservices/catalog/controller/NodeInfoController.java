package com.artivisi.training.microservices.catalog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class NodeInfoController {

    @Autowired private ObjectMapper objectMapper;

    @GetMapping("/api/nodeinfo")
    @PreAuthorize("hasAuthority('SCOPE_profile')")
    public Map<String, Object> nodeInfo(HttpServletRequest request, JwtAuthenticationToken currentUser) throws JsonProcessingException {
        Map<String, Object> info = new HashMap<>();
        info.put("Local IP", request.getLocalAddr());

        String username = (String) currentUser.getTokenAttributes().get("preferred_username");
        info.put("Authentication", currentUser);
        info.put("Username", username);
        return info;
    }
}
